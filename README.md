# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change).
We are using MongoDB, you can create that locally or using Azure CosmosDB, then provide properties in .env: MONGO_URL and MONGO_DB.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

If you dont have poetry on the command line, you can try running it directly from the install location, e.g.:
```bash
$ ~/.poetry/bin/poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Testing

We are using pytest, to run all the tests you can execute:
```bash
$ poetry run pytest
```
To run a specific test you can execute for example:
```bash
$ poetry run pytest test/test_view_model.py
```
To run Selenium end to end tests:
```bash
$ poetry run pytest test_e2e
```
The test is expecting you use Chrome browser, download the Chrome Selenium driver from here: https://chromedriver.storage.googleapis.com/index.html. If you are not using Chrome then you will need to download appropriate driver and change the driver method of selenium_test.py:
```python
def driver():
    with webdriver.Chrome() as driver:
        yield driver
```
To check code coverage, first run pytest with coverage:
```bash
$ poetry run coverage run -m pytest
```
Then generate a report:
```bash
$ poetry run coverage report -m
```
Or a more user friendly HTML report, which will be available from .\htmlcov\index.html:
```bash
$ poetry run coverage html
```

## Docker

If you haven't already, you'll need to install [Docker Desktop](https://www.docker.com/products/docker-desktop). Installation instructions for Windows can be found [here](https://docs.docker.com/docker-for-windows/install/).

### Production ready container

To build the production image:

```bash
$  docker build --target production --tag todo-app:prod .
```

To run the container:

```bash
$  docker run --env-file ./.env -p 80:80 todo-app:prod
```

This will make it available on default http port 80, if you need to run it in background use -d, e.g.:

```bash
$  docker run -d --env-file ./.env -p 80:80 todo-app:prod
```

### Testing locally

For convenience we have provided docker compose which starts flask version of application to allow for hot reload of code, it also starts a pytest-watch which will run the test suite if any py files are edited. To start this simply run:

```bash
$  docker compose up
```

This will make the app available on default http port 80.

If you want to run subset of tests in docker you can build the test image:

```bash
$  docker build --target test --tag test_image .
```

Then run the container passing the test name argument, e.g. for unit and integration tests:

```bash
$  docker run test_image test
```

Or for the e2e tests (requires valid .env file):

```bash
$  docker run --env-file ./.env test_image test_e2e
```

## Deployment

We are using Ansible to provision a machine and deploy the application. Follow the following steps:
- Edit the ./ansible/inventory file to include the managed nodes.
- Copy the files from ./ansible onto your control node
- From control node use ansible-vault to create env.yml to hold the TRELLO secrets:
```bash
$ ansible-vault create env.yml
```
- Enter a memorable password, then add the secrets in yml format, e.g.:
```yaml
MONGO_URL: YOUR_MONGO_URL
MONGO_DB: YOUR_MONGO_DB
```
- If you need to edit the file you can use:
```bash
$ ansible-vault edit env.yml
```
- Run the playbook, entering the vault password when prompted:
```bash
ansible-playbook playbook.yml -i inventory --ask-vault-pass
```
- Check app is running on managed node at port 80, e.g. http://13.41.238.207
- To check logs, from managed node run:
```bash
journalctl -u todoapp
```

## Continuous Deployment

The production image is published to [Docker Hub](https://hub.docker.com/repository/docker/j03t/todo_app) as part of Gitlab pipeline. The pipeline triggers the Azure Web App webhook to download the latest image. You can view the web app here: https://j03t-todo.azurewebsites.net/