import os


class Config:
    def __init__(self):
        """Base configuration variables."""
        for config in ['MONGO_URL', 'MONGO_DB']:
            config_val = os.environ.get(config)
            if not config_val:
                raise ValueError(f"No {config} set for Flask application. Did you follow the setup instructions?")