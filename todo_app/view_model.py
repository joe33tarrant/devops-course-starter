from datetime import date

class ViewModel:

    def __init__(self, items, show_hidden=False):
        self._items = items
        self.to_do = self.filter_status('To Do')
        self.doing = self.filter_status('Doing')
        all_done = self.filter_status('Done')
        self.show_hidden = show_hidden
        all_done.sort(key=lambda item: item.modified_date, reverse=True)
        if show_hidden or len(all_done) < 5:
            self.done = all_done
            self.has_hidden = False
        else:
            todays_done, older_done = [], []
            for item in all_done:
                if item.modified_date == date.today(): todays_done.append(item)
                else: older_done.append(item)
            if len(todays_done) > 0:
                self.done = todays_done
                self.has_hidden = len(older_done) > 0
            else:
                self.done = older_done[0:4]
                self.has_hidden = len(older_done) > 4

    @property
    def items(self):
        return {'To Do': self.to_do,
                'Doing': self.doing,
                'Done': self.done}

    def filter_status(self, status):
        return list(filter(lambda item: item.status == status, self._items))