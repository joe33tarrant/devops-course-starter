import os
from datetime import datetime

import pymongo
from bson.objectid import ObjectId

from todo_app.data.Item import Item


def todos():
    return pymongo.MongoClient(os.getenv('MONGO_URL'))[os.getenv('MONGO_DB')].todos

def get_items():
    items = []
    for todo in todos().find():
        items.append(Item(todo['_id'], todo['title'], todo['desc'], todo['due'], todo['modified_date'], todo['status']))
    return items

def add_item(title, desc, date):
    todos().insert_one({"title": title, "desc": desc, "due": date, "modified_date": datetime.today(), "status": "To Do"})

def set_status(id, status):
    update(id, {"status": status, "modified_date": datetime.today()})

def delete_item(id):
    todos().delete_one({"_id": ObjectId(id)}).deleted_count

def update_desc(id, desc):
    update(id, {"desc": desc, "modified_date": datetime.today()})

def update_due(id, due):
    update(id, {"due": due, "modified_date": datetime.today()})

def update(id, update):
    filter = {"_id": ObjectId(id)}
    todos().update_one(filter, {"$set": update})