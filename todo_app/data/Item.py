from datetime import datetime

class Item:

    def __init__(self, id, title, desc, due, modified_date, status='To Do'):
        self.id = id
        self.title = title
        self.status = status
        self.desc = desc
        self.due = due
        self.modified_date = modified_date