from flask import Flask, redirect, render_template, request

from todo_app import flask_config
from todo_app.data.mongo_items import *
from todo_app.view_model import ViewModel


def create_app():
    app = Flask(__name__)
    app.config.from_object(flask_config.Config())
    
    @app.route('/',  methods=["GET", "POST"])
    def index():
        items = sorted(get_items(), key=lambda item: item.status, reverse=True)
        show_hidden = request.args.get('show_hidden', default=False, type=bool)
        item_view_model = ViewModel(items, show_hidden)
        return render_template('index.html', view_model=item_view_model)

    @app.route("/add-new", methods=["POST"])
    def add_new():
        add_item(request.form.get('title'), request.form.get('desc'), request.form.get('date'))
        return redirect('/')

    def change_status(id, status):
        set_status(id, status)
        return redirect('/')

    @app.route("/start", methods=["POST"])
    def doing():
        return change_status(request.values.get('id'), 'Doing')

    @app.route("/done", methods=["POST"])
    def done():
        return change_status(request.values.get('id'), 'Done')

    @app.route("/delete", methods=["POST"])
    def delete():
        delete_item(request.values.get('id'))
        return redirect('/')

    @app.route("/desc", methods=["POST"])
    def desc():
        update_desc(request.values.get('id'), request.form.get('desc'))
        return redirect('/')

    @app.route("/due", methods=["POST"])
    def due():
        update_due(request.values.get('id'), request.form.get('date'))
        return redirect('/')
    
    return app