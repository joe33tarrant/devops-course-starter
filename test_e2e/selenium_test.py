import os
import sys
from threading import Thread
from time import sleep
from venv import create

import pymongo
import pytest
import requests
from dotenv import load_dotenv
from selenium import webdriver
from selenium.webdriver.common.by import By

from todo_app import app

BOARDS_URL = 'https://api.trello.com/1/boards'

@pytest.fixture(scope='module')
def app_with_temp_board():
    # Load our real environment variables
    load_dotenv(override=True)
    # Construct the new application
    application = app.create_app()
    # Start the app in its own thread.
    thread = Thread(target=lambda: application.run(use_reloader=False))
    thread.daemon = True
    thread.start()  
    # Give the app a moment to start
    sleep(1)
    # Return the application object as the result of the fixture
    yield application
    # Tear down
    thread.join(1)
    drop_db()

def drop_db():
    pymongo.MongoClient(os.getenv('MONGO_URL')).drop_database(os.getenv('MONGO_DB'))

@pytest.fixture(scope="module")
def driver():
    opts = webdriver.ChromeOptions()
    opts.add_argument('--headless')
    opts.add_argument('--no-sandbox')
    opts.add_argument('--disable-dev-shm-usage')
    with webdriver.Chrome(options=opts) as driver:
        yield driver

def test_task_journey(driver, app_with_temp_board):
    
    # date input is different for linux vs windows
    is_windows = sys.platform == "win32"

    driver.get('http://localhost:5000/')
    assert driver.title == "Joe's To-Do App"

    # Test adding new item with default title, no description or due date
    driver.find_element("xpath", "//*[@value='Submit']").click()
    todo_rows = driver.find_element(By.ID, "To Do").find_elements(By.TAG_NAME, "tr")
    assert len(todo_rows) == 2
    assert todo_rows[1].find_elements(By.TAG_NAME, "td")[1].text == 'New to do'
    assert todo_rows[1].find_element(By.ID, "desc").get_attribute("value") == ""
    assert todo_rows[1].find_element(By.ID, "due").get_attribute("value") == ""

    # Test update description
    todo_rows[1].find_element(By.ID, "desc").send_keys("a new description")
    todo_rows[1].find_elements("xpath", "//*[@value='update']")[0].click()
    todo_rows = driver.find_element(By.ID, "To Do").find_elements(By.TAG_NAME, "tr")
    assert todo_rows[1].find_element(By.ID, "desc").get_attribute("value") == "a new description"

    # Test update due date
    todo_rows[1].find_element(By.ID, "due").send_keys("25122033" if is_windows else "12252033")
    todo_rows[1].find_elements("xpath", "//*[@value='update']")[1].click()
    todo_rows = driver.find_element(By.ID, "To Do").find_elements(By.TAG_NAME, "tr")
    assert todo_rows[1].find_element(By.ID, "desc").get_attribute("value") == "a new description"
    assert todo_rows[1].find_element(By.ID, "due").get_attribute("value") == "2033-12-25"

    # Test Start
    todo_rows[1].find_element("xpath", "//*[@value='Start']").click()
    todo_rows = driver.find_element(By.ID, "To Do").find_elements(By.TAG_NAME, "tr")
    assert len(todo_rows) == 1
    doing_rows = driver.find_element(By.ID, "Doing").find_elements(By.TAG_NAME, "tr")
    assert len(doing_rows) == 2
    assert doing_rows[1].find_element(By.ID, "desc").get_attribute("value") == "a new description"
    assert doing_rows[1].find_element(By.ID, "due").get_attribute("value") == "2033-12-25"

    # Test Done
    doing_rows[1].find_element("xpath", "//*[@value='Done']").click()
    doing_rows = driver.find_element(By.ID, "Doing").find_elements(By.TAG_NAME, "tr")
    assert len(doing_rows) == 1
    done_rows = driver.find_element(By.ID, "Done").find_elements(By.TAG_NAME, "tr")
    assert len(done_rows) == 2
    assert done_rows[1].find_element(By.ID, "desc").get_attribute("value") == "a new description"
    assert done_rows[1].find_element(By.ID, "due").get_attribute("value") == "2033-12-25"

    # Test delete
    done_rows[1].find_element("xpath", "//*[@value='Delete']").click()
    todo_rows = driver.find_element(By.ID, "To Do").find_elements(By.TAG_NAME, "tr")
    doing_rows = driver.find_element(By.ID, "Doing").find_elements(By.TAG_NAME, "tr")
    done_rows = driver.find_element(By.ID, "Done").find_elements(By.TAG_NAME, "tr")
    assert len(todo_rows) == 1
    assert len(doing_rows) == 1
    assert len(done_rows) == 1

    # Test add new with description
    driver.find_element(By.ID, "new_title").clear()
    driver.find_element(By.ID, "new_title").send_keys("Second to do")
    driver.find_element(By.ID, "new_desc").send_keys("Lots of things to do")
    driver.find_element("xpath", "//*[@value='Submit']").click()
    todo_rows = driver.find_element(By.ID, "To Do").find_elements(By.TAG_NAME, "tr")
    assert len(todo_rows) == 2
    assert todo_rows[1].find_elements(By.TAG_NAME, "td")[1].text == 'Second to do'
    assert todo_rows[1].find_element(By.ID, "desc").get_attribute("value") == "Lots of things to do"
    assert todo_rows[1].find_element(By.ID, "due").get_attribute("value") == ""

    # Test add new with due date
    driver.find_element(By.ID, "new_title").clear()
    driver.find_element(By.ID, "new_title").send_keys("Third to do")
    driver.find_element(By.ID, "new_due").send_keys("14022044" if is_windows else "02142044")
    driver.find_element("xpath", "//*[@value='Submit']").click()
    todo_rows = driver.find_element(By.ID, "To Do").find_elements(By.TAG_NAME, "tr")
    assert len(todo_rows) == 3
    assert todo_rows[2].find_elements(By.TAG_NAME, "td")[1].text == 'Third to do'
    assert todo_rows[2].find_element(By.ID, "desc").get_attribute("value") == ""
    assert todo_rows[2].find_element(By.ID, "due").get_attribute("value") == "2044-02-14"