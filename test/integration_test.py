import os
from datetime import datetime

import mongomock
import pymongo
import pytest
from dotenv import find_dotenv, load_dotenv

from todo_app import app
from todo_app.data.mongo_items import todos

@pytest.fixture
def client():
    # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)
    
    with mongomock.patch(servers=(('fakemongo.com', 27017),)):
        # Create the new app.
        test_app = app.create_app()
        # Use the app to create a test_client that can be used in our tests.
        with test_app.test_client() as client:
            yield client

def test_index_page(client):
    todos().insert_one({"title": "A To Do item", "desc": "A To Do item description", "due": datetime.today(), "modified_date": datetime.today(), "status": "To Do"})
    todos().insert_one({"title": "A Doing item", "desc": "A Doing item description", "due": datetime.today(), "modified_date": datetime.today(), "status": "Doing"})
    todos().insert_one({"title": "A Done item", "desc": "A Done item description", "due": datetime.today(), "modified_date": datetime.today(), "status": "Done"})
    response = client.get('/')
    assert response.status_code == 200
    response_data = response.data.decode()
    assert 'A To Do item' in response_data
    assert 'A Doing item' in response_data
    assert 'A Done item' in response_data

def test_add_new(client):
    response = client.post('/add-new', data = {
        'title': 'test new to do',
        'desc': 'test new to do desc',
        'date': '2022-12-25'
    })
    assert todos().find_one({"title": "test new to do"})['desc'] == 'test new to do desc'

def test_start_todo(client):
    id = todos().insert_one({"title": "A To Do item", "desc": "A To Do item description", "due": datetime.today(), "modified_date": datetime.today(), "status": "To Do"}).inserted_id
    client.post('/start', data = {'id': id})
    assert todos().find_one({"_id": id})['status'] == 'Doing'
    
def test_todo_done(client):
    id = todos().insert_one({"title": "A To Do item", "desc": "A To Do item description", "due": datetime.today(), "modified_date": datetime.today(), "status": "To Do"}).inserted_id
    client.post('/done', data = {'id': id})
    assert todos().find_one({"_id": id})['status'] == 'Done'

def test_delete_todo(client):
    id = todos().insert_one({"title": "A To Do item", "desc": "A To Do item description", "due": datetime.today(), "modified_date": datetime.today(), "status": "To Do"}).inserted_id
    client.post('/delete', data = {'id': id})
    assert todos().find_one({"_id": id}) is None

def test_update_desc(client):
    id = todos().insert_one({"title": "A To Do item", "desc": "A To Do item description", "due": datetime.today(), "modified_date": datetime.today(), "status": "To Do"}).inserted_id
    client.post('/desc', data = {'id': id, 'desc': 'new desc'})
    assert todos().find_one({"_id": id})['desc'] == 'new desc'

def test_update_due(client):
    id = todos().insert_one({"title": "A To Do item", "desc": "A To Do item description", "due": datetime.today(), "modified_date": datetime.today(), "status": "To Do"}).inserted_id
    client.post('/due', data = {'id': id, 'date': '2022-12-25'})
    assert todos().find_one({"_id": id})['due'] == '2022-12-25'