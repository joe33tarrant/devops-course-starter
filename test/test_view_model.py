from datetime import date, timedelta
from todo_app.data.Item import Item
from todo_app.view_model import ViewModel

def test_status_filtering():
    ## Arrange
    to_do_item = Item(1, 'one', 'first', None, date.today())
    doing_item = Item(2, 'two', 'second', None, date.today(), 'Doing')
    done_item = Item(3, 'three', 'third', None, date.today(), 'Done')
    ## Act
    view_model = ViewModel([to_do_item, doing_item, done_item])
    ## Assert To Do
    assert len(view_model.items['To Do']) == 1
    assert view_model.items['To Do'][0] == to_do_item
    ## Assert Doing
    assert len(view_model.items['Doing']) == 1
    assert view_model.items['Doing'][0] == doing_item
    ## Assert Done
    assert len(view_model.items['Done']) == 1
    assert view_model.items['Done'][0] == done_item

def test_filter_old():
    view_model = ViewModel([
        Item(1, '1', '1', None, date.today() - timedelta(days=1), 'Done'),
        Item(2, '2', '2', None, date.today() - timedelta(days=1), 'Done'),
        Item(3, '3', '3', None, date.today() - timedelta(days=1), 'Done'),
        Item(4, '4', '4', None, date.today() - timedelta(days=1), 'Done'),
        Item(5, '5', '5', None, date.today() - timedelta(days=1), 'Done'),
        Item(6, '6', '6', None, date.today(), 'Done')])
    assert len(view_model.items['Done']) == 1
    assert view_model.items['Done'][0].id == 6
    assert view_model.has_hidden == True

def test_dont_filter_if_less_than_five_done():
    view_model = ViewModel([
        Item(1, '1', '1', None, date.today() - timedelta(days=1), 'Done'),
        Item(2, '2', '2', None, date.today() - timedelta(days=1), 'Done'),
        Item(3, '3', '3', None, date.today() - timedelta(days=1), 'Done'),
        Item(4, '4', '4', None, date.today() - timedelta(days=1), 'Done')])
    assert len(view_model.items['Done']) == 4
    assert view_model.has_hidden == False

def test_no_new_done():
    view_model = ViewModel([
        Item(1, '1', '1', None, date.today() - timedelta(days=1), 'Done'),
        Item(2, '2', '2', None, date.today() - timedelta(days=2), 'Done'),
        Item(3, '3', '3', None, date.today() - timedelta(days=3), 'Done'),
        Item(4, '4', '4', None, date.today() - timedelta(days=4), 'Done'),
        Item(5, '5', '5', None, date.today() - timedelta(days=5), 'Done'),])
    assert len(view_model.items['Done']) == 4
    assert view_model.items['Done'][0].id == 1
    assert view_model.items['Done'][1].id == 2
    assert view_model.items['Done'][2].id == 3
    assert view_model.items['Done'][3].id == 4
    assert view_model.has_hidden == True

def test_show_all_done():
    view_model = ViewModel([
        Item(1, '1', '1', None, date.today() - timedelta(days=2), 'Done'),
        Item(2, '2', '2', None, date.today() - timedelta(days=1), 'Done'),
        Item(3, '3', '3', None, date.today() - timedelta(days=3), 'Done'),
        Item(4, '4', '4', None, date.today() - timedelta(days=6), 'Done'),
        Item(5, '5', '5', None, date.today() - timedelta(days=5), 'Done'),], True)
    assert len(view_model.items['Done']) == 5
    assert view_model.items['Done'][0].id == 2
    assert view_model.items['Done'][4].id == 4
    assert view_model.has_hidden == False