FROM python:3.10-slim-buster as base

RUN apt-get update && apt-get install -y curl

RUN curl -sSL https://install.python-poetry.org | python3 -

RUN mkdir /app

COPY poetry.toml pyproject.toml poetry.lock /app/

COPY todo_app /app/todo_app

WORKDIR /app

RUN /root/.local/bin/poetry install --no-root --no-dev

EXPOSE 80

FROM base as production

CMD /root/.local/bin/poetry run gunicorn -w 1 -b 0.0.0.0:80 --log-file /app/gunicorn.log "todo_app.app:create_app()"

FROM base as development

CMD /root/.local/bin/poetry run flask run -h 0.0.0.0 -p 80

FROM base as test

RUN apt-get install -qqy wget gnupg unzip
# Install Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
  && apt-get update -qqy \
  && apt-get -qqy install google-chrome-stable \
  && rm /etc/apt/sources.list.d/google-chrome.list \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Install Chrome driver that is compatible with the installed version of Chrome
RUN CHROME_MAJOR_VERSION=$(google-chrome --version | sed -E "s/.* ([0-9]+)(\.[0-9]+){3}.*/\1/") \
  && CHROME_DRIVER_VERSION=$(wget --no-verbose -O - "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_${CHROME_MAJOR_VERSION}") \
  && echo "Using chromedriver version: "$CHROME_DRIVER_VERSION \
  && wget --no-verbose -O /tmp/chromedriver_linux64.zip https://chromedriver.storage.googleapis.com/$CHROME_DRIVER_VERSION/chromedriver_linux64.zip \
  && unzip /tmp/chromedriver_linux64.zip -d /usr/bin \
  && rm /tmp/chromedriver_linux64.zip \
  && chmod 755 /usr/bin/chromedriver

RUN /root/.local/bin/poetry install

COPY test /app/test

COPY test_e2e /app/test_e2e

COPY .env.test /app/.env.test

ENTRYPOINT ["/root/.local/bin/poetry", "run", "pytest"]